package org.marsik.elshelves.backend.test.config;

import org.marsik.elshelves.backend.app.spring.NoAutoscan;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@NoAutoscan
public class EmptyApplication {
}
