package org.marsik.elshelves.backend.services;

import org.marsik.elshelves.backend.controllers.exceptions.OperationNotPermitted;
import org.marsik.elshelves.backend.entities.User;
import org.marsik.elshelves.backend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends AbstractRestService<UserRepository, User> implements UserService {
	@Autowired
	public UserServiceImpl(UserRepository repository,
			UuidGenerator uuidGenerator) {
		super(repository, uuidGenerator);
	}

	@Override
	public User create(User dto, User currentUser) throws OperationNotPermitted {
		throw new OperationNotPermitted();
	}

    @Override
    protected Iterable<User> getAllEntities(User currentUser) {
        /* TODO XXX check if the currentUser has admin rights... */

        return getRepository().findAll();
    }
}
