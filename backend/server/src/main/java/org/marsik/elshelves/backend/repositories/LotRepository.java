package org.marsik.elshelves.backend.repositories;

import org.marsik.elshelves.backend.entities.Lot;

public interface LotRepository extends BaseOwnedEntityRepository<Lot> {
}
